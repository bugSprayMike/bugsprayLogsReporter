let process = require('process');
let bugsprayEvents = {};
let config = {};

bugsprayEvents.start = function(configObj) {
  config = configObj;
  console.log('child process started');
}

bugsprayEvents.save = function(saveObj) {

}

bugsprayEvents.stop = function() {
  process.disconnect();
}


process.on('message', (code) => {
  let eventName = code.event;
  let data = [(code.data ? code.data : {})];

  bugsprayEvents[eventName].apply(null, data);
});
